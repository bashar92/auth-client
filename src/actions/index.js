import { AUTH_USER, AUTH_ERR } from 'actions/types';
import axios from 'axios';

export const signup = (credentials, callback) => async (dispatch) => {
  // Call API
  axios
    .post('http://localhost:8085/api/signup', credentials)
    .then((res) => {
      dispatch({
        type: AUTH_USER,
        payload: res.data.token,
      });
      localStorage.setItem('token', res.data.token);
      callback();
    })
    .catch((err) => {
      dispatch({ type: AUTH_ERR, payload: 'Email in use!' });
    });
};

export const signin = (credentials, callback) => async (dispatch) => {
  axios
    .post('http://localhost:8085/api/signin', credentials)
    .then((res) => {
      dispatch({ type: AUTH_USER, payload: res.data.token });
      localStorage.setItem('token', res.data.token);
      callback();
    })
    .catch((err) =>
      dispatch({ type: AUTH_ERR, payload: 'Invalid Credentials' })
    );
};

export const signout = () => {
  localStorage.clear();
  return {
    type: AUTH_USER,
  };
};
