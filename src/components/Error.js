import React from 'react';
import 'stylesheets/error.css';

export default ({ msg }) => {
  const errDiv = msg ? <p className="error">{msg}</p> : null;

  return <>{errDiv}</>;
};
