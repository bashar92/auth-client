import React from 'react';
import { reduxForm, Field } from 'redux-form';
import 'stylesheets/forms.css';
import 'stylesheets/button.css';
import { compose } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'actions';
import Error from 'components/Error';

class Signup extends React.Component {
  onSubmit = (credentials) => {
    this.props.signup(credentials, () => {
      this.props.history.push('/feature');
    });
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="wrapper">
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <Error msg={this.props.auth.errMessage} />
          <fieldset>
            <label>Email</label>
            <Field
              name="email"
              autoComplete="none"
              type="text"
              component="input"
            />
          </fieldset>
          <fieldset>
            <label>Password</label>
            <Field name="password" type="password" component="input" />
          </fieldset>
          <button className="btnSubmit">Sign up</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'Signup' })
)(Signup);
