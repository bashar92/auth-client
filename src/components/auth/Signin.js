import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import 'stylesheets/forms.css';
import 'stylesheets/button.css';
import Error from 'components/Error';
import { Field, reduxForm } from 'redux-form';
import * as actions from 'actions';

class Signin extends React.Component {
  onSubmit = (credentials) => {
    this.props.signin(credentials, () => {
      this.props.history.push('/feature');
    });
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="wrapper">
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <Error msg={this.props.auth.errMessage} />
          <fieldset>
            <label>Email</label>
            <Field
              name="email"
              autoComplete="none"
              type="text"
              component="input"
            />
          </fieldset>
          <fieldset>
            <label>Password</label>
            <Field name="password" type="password" component="input" />
          </fieldset>
          <button className="btnSubmit">Sign in</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'sign in' })
)(Signin);
