import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import 'stylesheets/Navbar.css';

class Header extends Component {
  renderLinks = () => {
    if (this.props.authenticated) {
      return (
        <div>
          <li>
            <Link to="/signout">Sign out</Link>
          </li>
          <li>
            <Link to="/feature">Feature</Link>
          </li>
        </div>
      );
    } else {
      return (
        <div>
          <li>
            <Link to="/register">Sign up</Link>
          </li>
          <li>
            <Link to="/signin">Sign in</Link>
          </li>
        </div>
      );
    }
  };

  render() {
    return (
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        {this.renderLinks()}
      </ul>
    );
  }
}

const mapStateToProps = (state) => ({
  authenticated: state.auth.authenticated,
});

export default connect(mapStateToProps, null)(Header);
