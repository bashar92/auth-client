import { AUTH_USER, AUTH_ERR } from 'actions/types';

export default (
  state = {
    authenticated: '',
    errMessage: '',
  },
  action
) => {
  switch (action.type) {
    case AUTH_USER:
      return {
        ...state,
        authenticated: action.payload,
        errMessage: '',
      };
    case AUTH_ERR:
      return {
        ...state,
        errMessage: action.payload,
      };
    default:
      return state;
  }
};
